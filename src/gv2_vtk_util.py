"""vtk_util"""

import vtk
import numpy as np
from vtk.util.numpy_support import vtk_to_numpy

from gv2_path_and_filename import *

def check_gl_version_supported(renWin):
    extensions = vtk.vtkOpenGLExtensionManager()
    extensions.SetRenderWindow(renWin)
    print "GL_VERSION_1_2", extensions.ExtensionSupported("GL_VERSION_1_2")
    print "GL_VERSION_1_3", extensions.ExtensionSupported("GL_VERSION_1_3")
    print "GL_VERSION_1_4", extensions.ExtensionSupported("GL_VERSION_1_4")
    print "GL_VERSION_1_5", extensions.ExtensionSupported("GL_VERSION_1_5")
    print "GL_VERSION_2_0", extensions.ExtensionSupported("GL_VERSION_2_0")
    print "GL_VERSION_2_1", extensions.ExtensionSupported("GL_VERSION_2_1")
    print "GL_VERSION_3_0", extensions.ExtensionSupported("GL_VERSION_3_0")

# Capture the display and place in a tiff
def capture_image(renWin):
    w2i = vtk.vtkWindowToImageFilter()
    #writer = vtk.vtkTIFFWriter()
    writer = vtk.vtkPNGWriter()
    w2i.SetInput(renWin)
    w2i.Update()
    writer.SetInputConnection(w2i.GetOutputPort())
    filename = get_image_filename()
    writer.SetFileName(filename)
    renWin.Render()
    writer.Write()

def copy_volume_data(reader, volume_filename):
    #window = self.vtkWidget.GetRenderWindow()
    #vtk_win_im = vtk.vtkWindowToImageFilter()
    #vtk_win_im.SetInput(window)
    #vtk_win_im.Update()
    #vtk_image = vtk_win_im.GetOutput()    
    vtk_image = reader.GetOutput()
    height, width, depth = vtk_image.GetDimensions()
    vtk_array = vtk_image.GetPointData().GetScalars()
    components = vtk_array.GetNumberOfComponents()
    print "size=%d,%d,%d components=%d" % (height, width, depth, components)
    
    # get filenames for .mhd and .raw files
    numpy_array = vtk_to_numpy(vtk_array).reshape(height, width, depth)
    mhd_filename = extract_filename_from_path(volume_filename)
    raw_filename = mhd_filename.replace('.mhd', '.raw')
    path = create_directory_if_not_exist()

    # write data to .mhd and .raw files
    numpy_array.tofile(path + raw_filename)
    with open(volume_filename, 'r') as myfile, open(path + mhd_filename, 'w') as new:
        data = myfile.readlines()
        for line in data:
            new.write(line)

def get_mean(data, h, w, d, spacing):
    left = spacing / 2
    right = spacing / 2 + 1
    arr = data[h-left:h+right,w-left:w+right,d-left:d+right]
    return np.mean(arr)

def get_std(data, h, w, d, spacing):
    left = spacing / 2
    right = spacing / 2 + 1
    arr = data[h-left:h+right,w-left:w+right,d-left:d+right]
    return np.std(arr)

if __name__ == "__main__":
    print __doc__
