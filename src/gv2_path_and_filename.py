"""
path_and_filename

http://stackoverflow.com/questions/273192/check-if-a-path-exists-and-create-it-if-necessary
"""

import os
from PyQt4 import QtCore, QtGui, uic

def get_volume_filename(filename = None):
    if filename is None:
        filename = str(QtGui.QFileDialog.getOpenFileName(None, 'Select a volume data', '../data', "UNC MetaImage (*.mhd *.mha);; All Files (*)"))
        if len(filename ) < 1:
            filename = "../data/nucleon.mhd"
    return filename

def get_transfer_function_filename(filename = None):
    if filename is None:
        filename = str(QtGui.QFileDialog.getOpenFileName(None, 'Select a transfer function', '../transferfuncs', "Voreen transfer function (*.tfi);; All Files (*)"))
        if len(filename ) < 1:
            filename = "../transferfuncs/nucleon.tfi"
    return filename

def get_image_filename():
    path = create_directory_if_not_exist()
    filename = "image.png"
    return path + filename

def create_directory_if_not_exist(path = '../~/'):
    if not os.path.exists(path):
        os.makedirs(path)
    return path

def extract_filename_from_path(text):
    separator1 = '/'
    separator2 = '\\'
    index1 = text.rfind(separator1)
    index2 = text.rfind(separator2)
    index = max(index1, index2)
    return text[index+1:]

def append_separator_to_path(text):
    separator1 = '/'
    separator2 = '\\'
    index1 = text.rfind(separator1)
    index2 = text.rfind(separator2)
    result_string = text
    if index2 == -1:
        separator = separator1
    else:
        separator = separator2
    if index1 == -1 and index2 == -1:
        result_string = result_string + separator
    else:
        index = max(index1, index2)
        if index < len(result_string) - 1:
            result_string = result_string + separator
    return result_string

if __name__ == "__main__":
    print create_directory_if_not_exist()
