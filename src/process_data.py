"""process_data"""

import sys, os, csv, time
import numpy as np
import vtk
from vtk.util.numpy_support import vtk_to_numpy
from vtk.qt4.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor
import xml.etree.ElementTree as ET
from PyQt4 import QtCore, QtGui, uic
from PyQt4.QtCore import QObject, pyqtSignal, pyqtSlot
import matplotlib.pyplot as plt
from numba import autojit, jit

from gv2_transfer_function_plot import *
from gv2_path_and_filename import *
from gv2_vtk_util import *

def on_loadDataButton_clicked():
	#path = QtGui.QFileDialog.getExistingDirectory(None, "Open Directory", r'D:\_uchar\vortex', QtGui.QFileDialog.ShowDirsOnly | QtGui.QFileDialog.DontResolveSymlinks)
	path = QtGui.QFileDialog.getExistingDirectory(None, "Open Directory", r'D:\document\work\python-volume-rendering\~input', QtGui.QFileDialog.ShowDirsOnly | QtGui.QFileDialog.DontResolveSymlinks)
	if len(path) > 0:
		dir = QtCore.QDir(path)
		files = dir.entryList(['*.mhd'], QtCore.QDir.Files | QtCore.QDir.NoSymLinks)
		file_list = map(str, files)
		path = str(path)
		return path, file_list

def on_loadVolumeSeriesButton_clicked(path, file_list):
	path = append_separator_to_path(path)
	file_list_with_path = [path + filename for filename in file_list]
	data_list = []
	to_get_size = True
	
	for volume_filename in file_list_with_path:
		reader = vtk.vtkMetaImageReader()
		reader.SetFileName(volume_filename)
		reader.Update()
		vtk_image = reader.GetOutput()
		x, y, z = vtk_image.GetDimensions()
		vtk_array = vtk_image.GetPointData().GetScalars()
		components = vtk_array.GetNumberOfComponents()
		# only get size once
		if to_get_size:
			to_get_size = False
			size = (x, y, z)
		numpy_array = vtk_to_numpy(vtk_array).reshape(z, y, x) # numpy array is in C order by default (last-index varies the fastest)
		data_list.append(numpy_array)

	return size, data_list

def on_drawButton_clicked(size, data_list):
	print len(data_list)
	# Open File
	with open(create_directory_if_not_exist() + 'output.csv','wb') as csv_file:
		# Create Writer Object
		wr = csv.writer(csv_file, dialect='excel')
		height, width, depth = size
		print "size=%d,%d,%d" % (height, width, depth)
	
	spacing = 9
	start = spacing / 2 
	
	for d in range(start, depth, spacing):
		for w in range(start, width, spacing):
			for h in range(start, height, spacing):
				#line_data = [data[h,w,d] for data in data_list]
				line_data = [get_mean(data, h, w, d, spacing) for data in data_list]
				# Write Data to File
				wr.writerow(line_data)

##@autojit(nopython=True)
#def calculate_stats(size, data_list):
	#t_len = len(data_list)
	#print t_len
	#height, width, depth = size
	#print "size=%d,%d,%d" % (height, width, depth)
	#result = np.zeros_like(data_list)
	
	#spacing = 5
	#start = spacing / 2
	#left = spacing / 2
	#right = spacing / 2 + 1	
	#once = True
	
	#local_std = np.zeros((height, width, depth))
	#print local_std.shape
	#temporal_std = np.zeros((height, width, depth))
	#print temporal_std.shape
	
	#for d in range(depth):
	#for w in range(width):
		#for h in range(height):
		#if once:
			#h_left, h_right = get_left_and_right_indices(h-left,h+right,height)
			#w_left, w_right = get_left_and_right_indices(w-left,w+right,width)
			#d_left, d_right = get_left_and_right_indices(d-left,d+right,depth)
			#print 'h',h_left,h_right
			#print 'w',w_left,w_right
			#print 'd',d_left,d_right
			#y = [np.std(data[h_left:h+right,w_left:w_right,d_left:d_right]) for data in data_list]
			#print y
			
			#t_left, t_right = get_left_and_right_indices(0-left,0+right,t_len)
			#print 't',t_left,t_right
			#z = [data[h,w,d] for data in data_list[t_left:t_right]]
			#print z
			#temporal_std[h,w,d] = np.std(z)
			#print temporal_std[h,w,d]
			
			#once = False
			#break

@autojit(nopython=True)
def get_left_and_right_indices(left, right, length):
	while left < 0:
		left += 1
	while right > length:
		right -= 1

	return left, right

@autojit(nopython=True)
def variance(data):
	width, height, depth = data.shape
	#print 'data.shape',data.shape
	length = width * height * depth
	#if length<1:
	#print 'length =',length
	mean = 0.0
	var = 0.0
	if length > 1:
		for d in range(depth):
			for h in range(height):
				for w in range(width):
					mean += data[w,h,d]
		mean = mean / length
		for d in range(depth):
			for h in range(height):
				for w in range(width):
					var += np.square(data[w,h,d] - mean)
		var = var / length
	return var

@autojit(nopython=True)
def standard_deviation(data):
	width, height, depth = data.shape
	#print 'data.shape',data.shape
	length = width * height * depth
	#if length<1:
	#print 'length =',length
	mean = 0.0
	var = 0.0
	if length > 1:
		for d in range(depth):
			for h in range(height):
				for w in range(width):
					mean += data[w,h,d]

	mean = mean / length

	for d in range(depth):
		for h in range(height):
			for w in range(width):
				var += np.square(data[w,h,d] - mean)

	var = var / length
	return np.sqrt(var)

@autojit(nopython=True)
def calculate_local_jit(data, result):
	left = 4
	right = 1
	width, height, depth = data.shape
	for d in range(depth):
		for h in range(height):
			for w in range(width):
				w_left, w_right = get_left_and_right_indices(w-left,w+right,width)
				h_left, h_right = get_left_and_right_indices(h-left,h+right,height)
				d_left, d_right = get_left_and_right_indices(d-left,d+right,depth)
				# result[w,h,d] = np.var(data[h_left:h+right,w_left:w_right,d_left:d_right])
				# result[w,h,d] = variance(data[h_left:h+right,w_left:w_right,d_left:d_right])
				result[w,h,d] = standard_deviation(data[h_left:h+right,w_left:w_right,d_left:d_right])
	return result

def calculate_local(data_list):
	result_list = []
	for data in data_list:
		result = np.zeros(data.shape)
		result_list.append(calculate_local_jit(data, result))

	return result_list

def calculate_temporal(size, data_list):
	t_len = len(data_list)
	print t_len
	print "size=%d,%d,%d" % size
	spacing = 5
	left = spacing / 2
	right = spacing / 2 + 1
	once = True
	std_list = []
	width, height, depth = size
	for t in range(t_len):
		std = np.zeros(size)
		#print temporal_std.shape
		print t,'range',get_left_and_right_indices(t-left,t+right,t_len)
		t_left, t_right = get_left_and_right_indices(t-left,t+right,t_len)	
		for d in range(depth):
			for h in range(height):
				for w in range(width):
					z = [data[w,h,d] for data in data_list[t_left:t_right]]
					std[w,h,d] = np.var(z)
					#print temporal_std[w,h,d]

	std_list.append(std)
	return std_list

def write_data_to_files(path, file_list, data_list):
	for i in range(len(data_list)):
		d = data_list[i]
		m = np.amax(d)
		d = d / m * 255
		uchar = d.astype(dtype=np.uint8)
		print type(uchar), uchar.shape, uchar.dtype
		filename = '%03d.raw' % i
		uchar.tofile(create_directory_if_not_exist() + filename)
	
	path = append_separator_to_path(path)
	for i in range(len(file_list)):
		input_filename = path + file_list[i]
		output_filename = create_directory_if_not_exist() + ('%03d.mhd' % i)
		print 'filenames', input_filename, output_filename
		with open(input_filename, 'r') as myfile, open(output_filename, 'w') as new:
			data = myfile.readlines()
			for line in data:
				if -1 != line.find('ElementDataFile'):
					line = 'ElementDataFile = %03d.raw' % i
				new.write(line)

if __name__ == "__main__":
	print __doc__
	app = QtGui.QApplication(sys.argv)
	path, file_list = on_loadDataButton_clicked()
	print path, file_list
	size, data_list = on_loadVolumeSeriesButton_clicked(path, file_list)
	print size
	#print data_list
	t0 = time.time()
	data_list = calculate_local(data_list)
	t1 = time.time()
	print '%g seconds elapsed' % (t1-t0)
	print len(data_list), type(data_list[0]), data_list[0].shape, data_list[0].dtype
	write_data_to_files(path, file_list, data_list)
